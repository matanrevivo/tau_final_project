import torch
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
from sklearn import metrics

from neural_network.models import Siamese
from neural_network.train_multihead import load_embedding_matrix, pre_process, prepare_dataloader
from neural_network.params import TEST_TRUE_PAIRS, TEST_FALSE_PAIRS, ID2FUNC, LEN_MAX_SEQ, EMBEDDING_MATRIX, \
    DENSE_LAYER_SIZE, EMBEDDING_SIZE, D_WORD_VEC, D_MODEL, D_INNER, N_LAYERS, N_HEAD, D_K, D_V, DROPOUT

CHECKPOINT = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/out/checkpoints/model.chkpt"
LEARNING_CURVES = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/out/learning_curves"
AUC = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/out/auc"


def load_model():
    model = Siamese(load_embedding_matrix(EMBEDDING_MATRIX), len_max_seq=LEN_MAX_SEQ, dense_layer_size=DENSE_LAYER_SIZE,
                    embedding_size=EMBEDDING_SIZE,
                    d_word_vec=D_WORD_VEC, d_model=D_MODEL, d_inner=D_INNER, n_layers=N_LAYERS, n_head=N_HEAD, d_k=D_K,
                    d_v=D_V, dropout=DROPOUT)

    print("torch.cuda.is_available() = {}".format(torch.cuda.is_available()))
    if torch.cuda.is_available():
        checkpoint = torch.load(CHECKPOINT)
    else:
        checkpoint = torch.load(CHECKPOINT, map_location='cpu')
    model.load_state_dict(checkpoint['model'])
    model.eval()
    return model


def compute_roc(model):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.to(device)
    test_true_pairs = torch.load(TEST_TRUE_PAIRS)
    test_false_pairs = torch.load(TEST_FALSE_PAIRS)
    func_dict = torch.load(ID2FUNC)
    test_data = prepare_dataloader(test_true_pairs + test_false_pairs,
                                   [1] * len(test_true_pairs) + [-1] * len(test_false_pairs), func_dict)
    test_y = []
    test_prediction = []
    with torch.no_grad():
        for batch in tqdm(test_data, mininterval=2, desc='  - (Test) ', leave=False):
            # prepare data
            f1_seq, f1_pos, f2_seq, f2_pos, labels = map(lambda x: x.to(device), batch)
            predictions = model(f1_seq, f1_pos, f2_seq, f2_pos)
            test_y.extend(labels.tolist())
            test_prediction.extend(predictions.tolist())
    if np.isnan(test_prediction).any():
        print("Test: carefull there is  NaN in some ouput values, I am fixing it but be aware...")
        test_prediction = np.nan_to_num(test_prediction)
    test_fpr, test_tpr, _ = metrics.roc_curve(test_y, test_prediction, pos_label=1)
    roc_auc = metrics.auc(test_fpr, test_tpr) * 100

    fig = plt.figure()
    plt.plot(test_fpr, test_tpr, color='red', linewidth=1.2, label='Siamese Model (AUC = %0.2f%%)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='silver', linestyle=':', linewidth=1.2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate', fontsize=12)
    plt.ylabel('True Positive Rate', fontsize=12)
    plt.title('Receiver Operating Characteristic (ROC)')
    plt.legend(loc="lower right")
    fig.savefig(AUC)
    plt.close(fig)


def main():
    print("Loading model")
    model = load_model()
    print("Compute roc")
    compute_roc(model)
    print("Done!")


if __name__ == '__main__':
    main()
