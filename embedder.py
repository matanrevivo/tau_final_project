import os
import os.path
import torch
from argparse import ArgumentParser

from asm_embedding.FunctionAnalyzerRadare import RadareFunctionAnalyzer
from asm_embedding.FunctionNormalizer import FunctionNormalizer
from asm_embedding.InstructionsConverter import InstructionsConverter
from neural_network.params import LEN_MAX_SEQ, EMBEDDING_MATRIX
from neural_network.models import Siamese
from neural_network.train_multihead import load_embedding_matrix, pre_process


class Function_Embeder:
    def __init__(self, checkpoint):
        self.converter = InstructionsConverter("data/i2v/word2id.json")
        self.normalizer = FunctionNormalizer(max_instruction=LEN_MAX_SEQ)
        self.embedder = Siamese(load_embedding_matrix(EMBEDDING_MATRIX))
        self.load_weights(checkpoint)

    def load_weights(self, checkpoint):
        if torch.cuda.is_available():
            checkpoint = torch.load(checkpoint, 'model.chkpt')
        else:
            checkpoint = torch.load(checkpoint, map_location='cpu')
        self.embedder.load_state_dict(checkpoint['model'])
        self.embedder.eval()

    def address_to_func(self, filename, addr):
        analyzer = RadareFunctionAnalyzer(filename, use_symbol=False, depth=0)
        functions = analyzer.analyze()
        instructions_list = None
        for f in functions:
            if functions[f]['address'] == addr:
                instructions_list = functions[f]['filtered_instructions']
                break
        if instructions_list is None:
            print("Function not found")
            return None
        converted_instructions = self.converter.convert_to_ids(instructions_list)
        instructions, _ = self.normalizer.normalize_functions([converted_instructions])
        return instructions

    def embed_function(self, filename, addr):
        instructions = self.address_to_func(filename, addr)
        seq, pos = pre_process(instructions[0].tolist())
        seq, pos = seq.unsqueeze(0), pos.unsqueeze(0)
        func_embedding = self.embedder.forward_one(seq, pos)
        return func_embedding


if __name__ == '__main__':
    parser = ArgumentParser(description="Embedder")

    parser.add_argument("-c", "--checkpoint", help="Trained model checkpoint")
    parser.add_argument("-i", "--input", help="Input executable that contains the function to generate embedding")
    parser.add_argument("-a", "--address", help="Hexadecimal address of the function to generate embedding")

    args = parser.parse_args()

    address = int(args.address, 16)
    function_embeder = Function_Embeder(checkpoint=args.checkpoint)
    embedding = function_embeder.embed_function(args.input, address)
    print(embedding[0])
