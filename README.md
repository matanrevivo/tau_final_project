# Function Embeddings for Binary Similarity
   
Quickstart
-----
To create the embedding of a function:
```
git clone https://bitbucket.org/matanrevivo/tau_final_project
cd tau_final_project
wget https://www.dropbox.com/s/5pniuzxn9v8h846/data.zip
unzip data.zip
pip install -r requirements
python neural_network/setup.py install
git clone https://github.com/radare/radare2.git
./radare2/sys/install.sh
python embedder.py -c data/out/checkpoints/model.chkpt -i data/example/helloworld.o -a 100000F30
```
#### What to do with an embedding?
Once you have two embeddings ```embedding_x``` and ```embedding_y``` you can compute the similarity of the corresponding functions as: 
```
from sklearn.metrics.pairwise import cosine_similarity

similarity=cosine_similarity(embedding_x, embedding_y)
 
```

Train the model
---
If you want to train the model, first of all config the parameters in `neural_network/params.py`.
After you configured the training parameters you just need to execute:   
```
 python neural_network/neural_network/train_multihead.py
```

Code Explanation 
----

### asm_embedding
The folder is responsible for the preprocess of the raw data, it gets a binary and return a list of instructions index 
in the word2vec matrix  
**This code is taken from https://github.com/gadiluna/SAFE**

### dataset_creation
The folder is responsible for creating your own dataset from binaries.  
If you want to create your own dataset you can use the script `dataset_creation/ExperimentUtil.py`   
**This code is taken from https://github.com/gadiluna/SAFE**  

### neural_network
This folder contains my model and its training code.  
`models.py` and `layers.py` are the model  
`params.py` is the config file  
`train_multihead.py` is the training code



