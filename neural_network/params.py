# dataset
TRAIN_TRUE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/train_true_pairs"
TRAIN_FALSE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/train_false_pairs"
VALIDATION_TRUE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/validation_true_pairs"
VALIDATION_FALSE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/validation_false_pairs"
TEST_TRUE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/test_true_pairs"
TEST_FALSE_PAIRS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/test_true_pairs"


# word2vector
EMBEDDING_MATRIX = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/i2v/embedding_matrix.npy"
ID2FUNC = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/id2func"

# training parameters
BATCH_SIZE = 150
EPOCHS = 50
LR = 1e-3
LOG_FILE = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/out/log.txt"
CHECKPOINTS = "/mnt/c/D/DL/Tau_deep_learning/final_project/project/data/out/checkpoints"
SAVE_MODE = 'best'  # save modes are: 'all', 'best'

# network parameters
LEN_MAX_SEQ = 150
D_MODEL = 100
PAD = 0
DENSE_LAYER_SIZE = 1000
EMBEDDING_SIZE = 100
D_WORD_VEC = 100
D_INNER = 2048
N_LAYERS = 6
N_HEAD = 8
D_K = 64
D_V = 64
DROPOUT = 0.1
