import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F

from neural_network.layers import EncoderLayer
from neural_network.params import PAD


def get_sinusoid_encoding_table(n_position, d_hid, padding_idx=None):
    """
    Sinusoid position encoding table
    """

    def cal_angle(position, hid_idx):
        return position / np.power(10000, 2 * (hid_idx // 2) / d_hid)

    def get_position_angle_vec(position):
        return [cal_angle(position, hid_j) for hid_j in range(d_hid)]

    sinusoid_table = np.array([get_position_angle_vec(pos_i) for pos_i in range(n_position)])
    sinusoid_table[:, 0::2] = np.sin(sinusoid_table[:, 0::2])  # dim 2i
    sinusoid_table[:, 1::2] = np.cos(sinusoid_table[:, 1::2])  # dim 2i+1
    if padding_idx is not None:
        # zero vector for padding dimension
        sinusoid_table[padding_idx] = 0.
    return torch.FloatTensor(sinusoid_table)


def get_non_pad_mask(seq):
    assert seq.dim() == 2
    return seq.ne(PAD).type(torch.float).unsqueeze(-1)


def get_attn_key_pad_mask(seq_k, seq_q):
    """
    For masking out the padding part of key sequence.
    """
    # Expand to fit the shape of key query attention matrix.
    len_q = seq_q.size(1)
    padding_mask = seq_k.eq(PAD)
    padding_mask = padding_mask.unsqueeze(1).expand(-1, len_q, -1)  # b x lq x lk

    return padding_mask


class Transformer_Encoder(nn.Module):
    """
    Encoder model with self attention mechanism.
    """

    def __init__(
            self, word2vec, len_max_seq, d_word_vec,
            n_layers, n_head, d_k, d_v,
            d_model, d_inner, dropout=0.1):
        super().__init__()

        n_position = len_max_seq + 1

        self.src_word_emb = nn.Embedding.from_pretrained(word2vec, freeze=True)

        self.position_enc = nn.Embedding.from_pretrained(
            get_sinusoid_encoding_table(n_position, d_word_vec, padding_idx=0), freeze=True)

        self.layer_stack = nn.ModuleList([
            EncoderLayer(d_model, d_inner, n_head, d_k, d_v, dropout=dropout)
            for _ in range(n_layers)])

    def forward(self, src_seq, src_pos):
        # -- Prepare masks
        slf_attn_mask = get_attn_key_pad_mask(seq_k=src_seq, seq_q=src_seq)
        non_pad_mask = get_non_pad_mask(src_seq)

        # -- Forward
        enc_output = self.src_word_emb(src_seq) + self.position_enc(src_pos)

        for enc_layer in self.layer_stack:
            enc_output, enc_slf_attn = enc_layer(
                enc_output,
                non_pad_mask=non_pad_mask,
                slf_attn_mask=slf_attn_mask)
        return enc_output


class Embedder(nn.Module):
    """
    A sequence to sequence model with attention mechanism.
    """

    def __init__(
            self, word2vec, len_max_seq=150, dense_layer_size=1000, embedding_size=200,
            d_word_vec=100, d_model=100, d_inner=2048,
            n_layers=6, n_head=8, d_k=64, d_v=64, dropout=0.1):
        super().__init__()

        assert d_model == d_word_vec, 'To facilitate the residual connections, the dimensions of all module outputs ' \
                                      'shall be the same. '

        self.len_max_seq = len_max_seq
        self.d_model = d_model

        self.encoder = Transformer_Encoder(word2vec, len_max_seq=len_max_seq, d_word_vec=d_word_vec, d_model=d_model,
                                           d_inner=d_inner, n_layers=n_layers, n_head=n_head, d_k=d_k, d_v=d_v,
                                           dropout=dropout)
        self.fc1 = nn.Linear(d_model * len_max_seq, dense_layer_size)
        self.fc2 = nn.Linear(dense_layer_size, embedding_size)

    def forward(self, src_seq, src_pos):
        enc_output = self.encoder(src_seq, src_pos)
        embedding = enc_output.view(-1, self.len_max_seq * self.d_model)
        embedding = F.relu(self.fc1(embedding))
        embedding = self.fc2(embedding)
        return embedding


class Siamese(nn.Module):
    def __init__(self, word2vec, len_max_seq=150, dense_layer_size=1000, embedding_size=100, d_word_vec=100,
                 d_model=100, d_inner=2048, n_layers=6, n_head=8, d_k=64, d_v=64, dropout=0.1):
        super().__init__()
        self.cosine = nn.CosineSimilarity(dim=1, eps=1e-6)
        self.embedder = Embedder(word2vec, len_max_seq, dense_layer_size, embedding_size,
                                 d_word_vec, d_model, d_inner,
                                 n_layers, n_head, d_k, d_v, dropout)

    def forward_one(self, seq, pos):
        out = self.embedder(seq, pos)
        return out

    def forward(self, seq1, pos1, seq2, pos2):
        out1 = self.forward_one(seq1, pos1)
        out2 = self.forward_one(seq2, pos2)
        out = self.cosine(out1, out2)
        return out
