import os
import os.path
import sys
import time
import random
import torch
import torch.nn as nn
import torch.utils.data
from torch import autograd
import numpy as np
from tqdm import tqdm
from sklearn import metrics

from neural_network.models import Siamese
from neural_network.params import EMBEDDING_MATRIX, ID2FUNC, PAD, TRAIN_TRUE_PAIRS, TRAIN_FALSE_PAIRS, VALIDATION_TRUE_PAIRS, \
    VALIDATION_FALSE_PAIRS, BATCH_SIZE, LEN_MAX_SEQ, LOG_FILE, SAVE_MODE, CHECKPOINTS, EPOCHS, LR, D_MODEL, \
    DENSE_LAYER_SIZE, EMBEDDING_SIZE, D_WORD_VEC, D_INNER, N_LAYERS, N_HEAD, D_K, D_V, DROPOUT


def load_embedding_matrix(matrix_path):
    if os.path.isfile(matrix_path):
        try:
            with open(matrix_path, 'rb') as f:
                return torch.from_numpy(np.float32(np.load(f)))
        except Exception as e:
            print("Exception handling file:" + str(matrix_path))
            print("Embedding matrix cannot be load")
            print(str(e))
            sys.exit(-1)
    else:
        print('Embedding matrix not found at path:' + str(matrix_path))
        sys.exit(-1)


def pre_process(f, max_len=LEN_MAX_SEQ):
    truncate_f = f[0:max_len]
    pad = [PAD] * (max_len - len(truncate_f))
    padded = truncate_f[:] + pad
    pos = list(range(1, len(truncate_f) + 1)) + pad
    return torch.Tensor(padded).long(), torch.Tensor(pos).long()


class SimilaritiesDataset(torch.utils.data.Dataset):
    def __init__(self, func_pairs, labels, idx2func):
        assert len(func_pairs) == len(labels)
        self.func_pairs = func_pairs
        self.labels = labels
        self.idx2func = idx2func

    def __len__(self):
        return len(self.func_pairs)

    def __getitem__(self, idx):
        f1_idx, f2_idx = self.func_pairs[idx]
        f1, f2 = self.idx2func[f1_idx], self.idx2func[f2_idx]
        f1_seq, f1_pos = pre_process(f1)
        f2_seq, f2_pos = pre_process(f2)
        return f1_seq, f1_pos, f2_seq, f2_pos, self.labels[idx]


def prepare_dataloader(func_pairs, labels, idx2func):
    return torch.utils.data.DataLoader(SimilaritiesDataset(func_pairs, labels, idx2func),
                                       num_workers=2,
                                       batch_size=BATCH_SIZE,
                                       shuffle=True)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def train_epoch(model, train_data, optimizer, criterion, device):
    model.train()

    total_loss = 0
    train_y = []
    train_prediction = []
    print("train_data.batch_size = {}".format(train_data.batch_size))
    print("len(train_data.dataset) = {}".format(len(train_data.dataset)))
    for batch in tqdm(train_data, mininterval=2, desc='  - (Training)   ', leave=False):
        # prepare data
        f1_seq, f1_pos, f2_seq, f2_pos, labels = map(lambda x: x.to(device), batch)

        # forward
        optimizer.zero_grad()
        predictions = model(f1_seq, f1_pos, f2_seq, f2_pos)

        # backward
        with autograd.detect_anomaly():
            loss = criterion(predictions, labels.float())
            total_loss += loss.item() if not np.isnan(loss.item()) else 0
            loss.backward()

        # update parameters
        optimizer.step()

        # note keeping
        train_y.extend(labels.tolist())
        train_prediction.extend(predictions.tolist())

    avg_loss = total_loss / len(train_data.dataset)
    if np.isnan(train_prediction).any():
        with open(LOG_FILE, 'a') as f:
            f.write("Train: carefull there is  NaN in some ouput values, I am fixing it but be aware...\n")
        print("Train: carefull there is  NaN in some ouput values, I am fixing it but be aware...")
        train_prediction = np.nan_to_num(train_prediction)
    test_fpr, test_tpr, _ = metrics.roc_curve(train_y, train_prediction, pos_label=1)
    roc_auc = metrics.auc(test_fpr, test_tpr)
    return avg_loss, roc_auc


def eval_epoch(model, test_data, criterion, device):
    model.eval()

    total_loss = 0
    test_y = []
    test_prediction = []

    print("len(test_data.dataset) = {}".format(len(test_data.dataset)))
    with torch.no_grad():
        for batch in tqdm(test_data, mininterval=2, desc='  - (Test) ', leave=False):
            # prepare data
            f1_seq, f1_pos, f2_seq, f2_pos, labels = map(lambda x: x.to(device), batch)

            # forward
            predictions = model(f1_seq, f1_pos, f2_seq, f2_pos)
            loss = criterion(predictions, labels.float())

            # note keeping
            total_loss += loss.item() if not np.isnan(loss.item()) else 0
            test_y.extend(labels.tolist())
            test_prediction.extend(predictions.tolist())

    avg_loss = total_loss / len(test_data.dataset)
    if np.isnan(test_prediction).any():
        with open(LOG_FILE, 'a') as f:
            f.write("Test: carefull there is  NaN in some ouput values, I am fixing it but be aware...\n")
        print("Test: carefull there is  NaN in some ouput values, I am fixing it but be aware...")
        test_prediction = np.nan_to_num(test_prediction)
    test_fpr, test_tpr, _ = metrics.roc_curve(test_y, test_prediction, pos_label=1)
    roc_auc = metrics.auc(test_fpr, test_tpr)
    return avg_loss, roc_auc


def train(model, train_true_pairs, train_false_pairs, func_dict, test_data, optimizer, criterion, device):
    print('Training log will be written to file: {}'.format(LOG_FILE))
    with open(LOG_FILE, 'a') as f:
        f.write('epoch,loss,ppl,accuracy\n')

    train_losses = []
    test_losses = []
    train_accuracies = []
    test_accuracies = []
    pretrain = False
    train_loss = -1
    train_accu = -1

    # load pretrain model weights
    if os.path.exists(os.path.join(CHECKPOINTS, 'model.chkpt')):
        print("loading checkpoint...")
        if torch.cuda.is_available():
            checkpoint = torch.load(os.path.join(CHECKPOINTS, 'model.chkpt'))
        else:
            checkpoint = torch.load(os.path.join(CHECKPOINTS, 'model.chkpt'), map_location='cpu')
        model.load_state_dict(checkpoint['model'])
        pretrain = True

    for epoch_i in range(EPOCHS):
        print('[ Epoch', epoch_i, ']')
        # --------- Create train dataloader -----------
        # used to limit the train epoch time
        true_pairs_samples = random.sample(train_true_pairs, len(test_data.dataset) * 10)
        false_pairs_samples = random.sample(train_false_pairs, len(test_data.dataset) * 10)

        train_data = prepare_dataloader(true_pairs_samples + false_pairs_samples,
                                        [1] * len(true_pairs_samples) + [-1] * len(false_pairs_samples), func_dict)

        # --------- Train Epoch -----------------------
        if not (epoch_i == 0 and pretrain):
            start = time.time()
            train_loss, train_accu = train_epoch(model, train_data, optimizer, criterion, device)
            print('  - (Training)   loss: {loss: 8.5f}, accuracy: {accu:3.3f} %, elapse: {elapse:3.3f} min'.format(
                loss=train_loss, accu=100 * train_accu, elapse=(time.time() - start) / 60))
            train_losses.append(train_loss)
            train_accuracies.append(train_accu)

        # --------- Test Eval -------------
        start = time.time()
        test_loss, test_accu = eval_epoch(model, test_data, criterion, device)
        print('  - (Validation) loss: {loss: 8.5f}, accuracy: {accu:3.3f} %, elapse: {elapse:3.3f} min'.format(
            loss=test_loss, accu=100 * test_accu, elapse=(time.time() - start) / 60))
        test_losses.append(test_loss)
        test_accuracies.append(test_accu)

        model_state_dict = model.state_dict()
        checkpoint = {'model': model_state_dict, 'epoch': epoch_i}

        # --------- Log accuracy -------
        with open(LOG_FILE, 'a') as f:
            f.write('  - (Training)   {epoch},{loss: 8.5f},{accu:3.3f}\n'.format(epoch=epoch_i, loss=train_loss,
                                                                                 accu=100 * train_accu))
            f.write('  - (Testing)    {epoch},{loss: 8.5f},{accu:3.3f}\n'.format(epoch=epoch_i, loss=test_loss,
                                                                                 accu=100 * test_accu))

        # --------- Save checkpoint -------
        if SAVE_MODE == 'all':
            model_name = os.path.join(CHECKPOINTS, 'model_accu_{accu:3.3f}.chkpt'.format(accu=100 * test_accu))
            torch.save(checkpoint, model_name)
        elif SAVE_MODE == 'best':
            model_name = os.path.join(CHECKPOINTS, 'model.chkpt')
            if test_accu >= max(test_accuracies):
                torch.save(checkpoint, model_name)
                print('    - [Info] The checkpoint file has been updated, test_accu = {}'.format(test_accu))
                with open(LOG_FILE, 'a') as f:
                    f.write('    - [Info] The checkpoint file has been updated, test_accu = {} \n'.format(test_accu))


def main():
    # --------- Loading Dataset -----------
    print("loading dataset")
    func_dict = torch.load(ID2FUNC)
    train_true_pairs = torch.load(TRAIN_TRUE_PAIRS)
    train_false_pairs = torch.load(TRAIN_FALSE_PAIRS)
    test_true_pairs = torch.load(VALIDATION_TRUE_PAIRS)
    test_false_pairs = torch.load(VALIDATION_FALSE_PAIRS)

    print("prepate dataloader")
    test_data = prepare_dataloader(test_true_pairs + test_false_pairs,
                                   [1] * len(test_true_pairs) + [-1] * len(test_false_pairs), func_dict)
    # --------- Preparing Model -----------
    print("loading embedding matrix")
    word2vec = load_embedding_matrix(EMBEDDING_MATRIX)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = Siamese(word2vec, len_max_seq=LEN_MAX_SEQ, dense_layer_size=DENSE_LAYER_SIZE, embedding_size=EMBEDDING_SIZE,
                    d_word_vec=D_WORD_VEC, d_model=D_MODEL, d_inner=D_INNER, n_layers=N_LAYERS, n_head=N_HEAD, d_k=D_K,
                    d_v=D_V, dropout=DROPOUT).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)

    criterion = nn.MSELoss()
    print("num_of_params={}".format(count_parameters(model)))
    # --------- Training model -----------
    train(model, train_true_pairs, train_false_pairs, func_dict, test_data, optimizer, criterion, device)


if __name__ == "__main__":
    main()
